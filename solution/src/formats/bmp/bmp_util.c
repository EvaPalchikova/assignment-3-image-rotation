#include "../../../includes/formats/bmp/bmp_util.h"
#include "../../../includes/util.h"

#define BLOCK_SIZE 4
//Если ширина не кратна четырём, то она дополняется мусорными байтами до ближайшего числа, кратного четырём.

uint32_t calculate_padding(const uint32_t width) {
    uint32_t width_size = (sizeof(struct pixel)) * width;
    uint32_t in_last_block = width_size % BLOCK_SIZE;
    return (BLOCK_SIZE - in_last_block) % BLOCK_SIZE;
}
