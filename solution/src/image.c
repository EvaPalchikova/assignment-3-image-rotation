#include "../includes/image.h"
#include "../includes/errors.h"

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image *new_image(const uint32_t width, const uint32_t height) {
    struct image *image = malloc(sizeof(struct image));
    if (image == NULL) {
        out_of_memory_error();
        return NULL;
    }
    image->width = width;
    image->height = height;
    image->data = malloc(width * height * sizeof(struct pixel));
    if (image->data == NULL) {
        out_of_memory_error();
    }
    return image;
}

uint32_t image_get_width(const struct image *image) {
    return image->width;
}

uint32_t image_get_height(const struct image *image) {
    return image->height;
}

void image_free(struct image *image) {
    free(image->data);
    free(image);
}

struct pixel *row_ptr(const struct image *image, const uint32_t i) {
    return image->data + i * image->width;
}

uint32_t row_size(const struct image *image) {
    return image->width * sizeof(struct pixel);
}

struct pixel get_pixel(const struct image *image, const uint32_t i, const uint32_t j) {
    return *(image->data + i + j * image->width);
}

void set_pixel(struct image *image, const uint32_t i, const uint32_t j, const struct pixel pixel) {
    *(image->data + i + j * image->width) = pixel;
}


