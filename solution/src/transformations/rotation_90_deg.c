#include "../../includes/transformations/rotation_90_deg.h"
#include "../../includes/image.h"
#include "../../includes/util.h"
#include <malloc.h>

typedef void calculate_transformed_width_and_height(uint64_t, uint64_t, uint64_t *, uint64_t *);

static void change_width_and_height(const uint64_t source_width,
                                    const uint64_t source_height,
                                    uint64_t *transformed_width,
                                    uint64_t *transformed_height) {
    *transformed_width = source_height;
    *transformed_height = source_width;
}

static void not_change_width_and_height(const uint64_t source_width,
                                        const uint64_t source_height,
                                        uint64_t *transformed_width,
                                        uint64_t *transformed_height) {
    *transformed_width = source_width;
    *transformed_height = source_height;
}

typedef void calculate_source_coords(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t *, uint64_t *);

static void calculate_source_0(const uint64_t i, const uint64_t j,
                               const uint64_t width, const uint64_t height,
                               uint64_t *source_i,
                               uint64_t *source_j) {
    (void) width;
    (void) height;
    *source_i = i;
    *source_j = j;
}

void calculate_source_90(const uint64_t i, const uint64_t j,
                         const uint64_t width, const uint64_t height,
                         uint64_t *source_i,
                         uint64_t *source_j) {
    (void) width;
    *source_i = height - j - 1;
    *source_j = i;
}

void calculate_source_180(const uint64_t i, const uint64_t j,
                          const uint64_t width, const uint64_t height,
                          uint64_t *source_i,
                          uint64_t *source_j) {
    *source_i = width - i - 1;
    *source_j = height - j - 1;
}

void calculate_source_270(const uint64_t i, const uint64_t j,
                          const uint64_t width, const uint64_t height,
                          uint64_t *source_i,
                          uint64_t *source_j) {
    (void) height;
    *source_i = j;
    *source_j = width - i - 1;
}

static void choose_functions(const enum degree deg,
                             calculate_transformed_width_and_height **calculate_transformed_width_and_height,
                             calculate_source_coords **calculate_source_coords) {
    switch (deg) {
        case A_0:
            *calculate_transformed_width_and_height = not_change_width_and_height;
            *calculate_source_coords = calculate_source_0;
            break;
        case A_90:
        case A_m270:
            *calculate_transformed_width_and_height = change_width_and_height;
            *calculate_source_coords = calculate_source_90;
            break;
        case A_180:
        case A_m180:
            *calculate_transformed_width_and_height = not_change_width_and_height;
            *calculate_source_coords = calculate_source_180;
            break;
        case A_270:
        case A_m90:
            *calculate_transformed_width_and_height = change_width_and_height;
            *calculate_source_coords = calculate_source_270;
    }
}

struct image *rotate(struct image *source, const enum degree deg) {
    calculate_transformed_width_and_height *calculate_transformed_width_and_height;
    calculate_source_coords *calculate_source_coords;
    choose_functions(deg, &calculate_transformed_width_and_height, &calculate_source_coords);

    uint64_t transformed_width, transformed_height;
    calculate_transformed_width_and_height(
            image_interface.get_width(source),
            image_interface.get_height(source),
            &transformed_width, &transformed_height);
    struct image *transformed = image_interface.create(transformed_width, transformed_height);

    uint64_t source_i, source_j;
    for (size_t i = 0; i < transformed_width; i++) {
        for (size_t j = 0; j < transformed_height; j++) {
            calculate_source_coords(i, j, transformed_width, transformed_height, &source_i, &source_j);
            set_pixel(transformed, i, j, image_interface.get_pixel(source, source_i, source_j));
        }
    }
    return transformed;
}

