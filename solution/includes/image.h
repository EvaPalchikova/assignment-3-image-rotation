#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include "util.h"
#include <inttypes.h>
#include <malloc.h>


struct image *new_image(uint32_t width, uint32_t height);

uint32_t image_get_width(const struct image *image);

uint32_t image_get_height(const struct image *image);

struct pixel *row_ptr(const struct image *image, uint32_t i);

uint32_t row_size(const struct image *image);

struct pixel get_pixel(const struct image *image, uint32_t i, uint32_t j);

void set_pixel(struct image *image, uint32_t i, uint32_t j, struct pixel pixel);

void image_free(struct image *image);

static struct image_interface {
    struct image *(*create)(uint32_t, uint32_t);

    uint32_t (*get_width)(const struct image *);

    uint32_t (*get_height)(const struct image *);

    struct pixel *(*row_ptr)(const struct image *, uint32_t);

    uint32_t (*row_size)(const struct image *);

    struct pixel (*get_pixel)(const struct image *, uint32_t, uint32_t);

    void (*set_pixel)(struct image *, uint32_t, uint32_t, const struct pixel);

    void (*free)(struct image *);

} const image_interface = {
        new_image,
        image_get_width,
        image_get_height,
        row_ptr,
        row_size,
        get_pixel,
        set_pixel,
        image_free,
};

#endif //IMAGE_TRANSFORMER_IMAGE_H
