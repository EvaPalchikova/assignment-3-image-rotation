#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "../../util.h"


struct image *from_bmp(FILE *in);

void to_bmp(FILE *out, struct image *image);

#endif //IMAGE_TRANSFORMER_BMP_H
