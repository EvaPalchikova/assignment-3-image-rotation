#include "../../../includes/formats/bmp/bmp_header.h"
#include "../../../includes/errors.h"

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bmp_header *new_empty_bmp_header(void) {
    struct bmp_header *header = malloc(sizeof(struct bmp_header));
    if (header == NULL) {
        out_of_memory_error();
    }
    return header;
}

struct bmp_header *new_bmp_header(const uint32_t width, const uint32_t height) {
    struct bmp_header *header = new_empty_bmp_header();

    uint32_t header_size = sizeof(struct bmp_header);
    uint32_t image_size =
            (width + calculate_padding(width)) * height * sizeof(struct pixel) + calculate_padding(width * height);

    header->bfType = BF_TYPE;
    header->bfileSize = image_size + header_size;
    header->bfReserved = BF_RESERVED;
    header->bOffBits = header_size;
    header->biSize = BI_SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BI_PLANES;
    header->biBitCount = BI_BIT_COUNT;
    header->biCompression = BI_COMPRESSION;
    header->biSizeImage = image_size;
    header->biXPelsPerMeter = BI_X_PIXELS_PER_METER;
    header->biYPelsPerMeter = BI_Y_PIXELS_PER_METER;
    header->biClrUsed = BI_CLR_USED;
    header->biClrImportant = BI_CLR_IMPORTANT;

    return header;
}

uint32_t bmp_header_get_width(struct bmp_header *header) {
    return header->biWidth;
}

uint32_t bmp_header_get_height(struct bmp_header *header) {
    return header->biHeight;
}

size_t get_header_size(void) {
    return sizeof(struct bmp_header);
}

void free_header(struct bmp_header *header) {
    free(header);
}
