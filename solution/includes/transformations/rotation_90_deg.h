#ifndef IMAGE_TRANSFORMER_ROTATION_90_DEG_H
#define IMAGE_TRANSFORMER_ROTATION_90_DEG_H


enum degree {
    A_0 = 0,
    A_90 = 90,
    A_m90 = -90,
    A_180 = 180,
    A_m180 = -180,
    A_270 = 270,
    A_m270 = -270
};

struct image *rotate(struct image *source, enum degree deg);


#endif //IMAGE_TRANSFORMER_ROTATION_90_DEG_H
