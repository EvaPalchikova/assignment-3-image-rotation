#ifndef IMAGE_TRANSFORMER_UTIL_H
#define IMAGE_TRANSFORMER_UTIL_H

#include <bits/types/FILE.h>
#include <inttypes.h>
#include <stdbool.h>

enum open_mode {
    READ, WRITE
};

FILE *open_file(const char *filename, enum open_mode mode);

void close_file(FILE *f);

struct __attribute__((packed)) pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

#endif //IMAGE_TRANSFORMER_UTIL_H
