#ifndef IMAGE_TRANSFORMER_BMP_UTIL_H
#define IMAGE_TRANSFORMER_BMP_UTIL_H

#include <inttypes.h>

uint32_t calculate_padding(uint32_t width);

#endif //IMAGE_TRANSFORMER_BMP_UTIL_H
