#include "../../../includes/formats/bmp/bmp.h"
#include "../../../includes/errors.h"
#include "../../../includes/formats/bmp/bmp_header.h"
#include "../../../includes/image.h"
#include <malloc.h>
#include <stdio.h>


static void read_width_and_height(FILE *in, uint32_t *width, uint32_t *height) {
    struct bmp_header *header = bmp_header_interface.create_empty();
    size_t count = fread(header, bmp_header_interface.get_size(), 1, in);
    if (count != 1) {
        read_header_error();
    }
    *width = bmp_header_interface.get_width(header);
    *height = bmp_header_interface.get_height(header);
    bmp_header_interface.free(header);
}

static void read_data(FILE *in, struct image *image) {
    uint32_t padding_size = calculate_padding(image_interface.get_width(image));
    uint8_t *padding = malloc(padding_size);
    if (padding == NULL) {
        out_of_memory_error();
    }

    for (size_t i = 0; i < image_interface.get_height(image); i++) {
        size_t count = fread(
                image_interface.row_ptr(image, i),
                image_interface.row_size(image),
                1, in);
        if (count != 1) {
            read_data_error();
        }
        fread(padding, padding_size, 1, in);
    }

    free(padding);
}

struct image *from_bmp(FILE *in) {
    uint32_t width, height;
    read_width_and_height(in, &width, &height);
    struct image *image = image_interface.create(width, height);
    read_data(in, image);
    return image;
}

static void write_header(FILE *out, uint32_t width, uint32_t height) {
    struct bmp_header *header = bmp_header_interface.create(width, height);
    size_t count = fwrite(header, bmp_header_interface.get_size(), 1, out);
    if (count != 1) {
        write_header_error();
    }
    bmp_header_interface.free(header);
}

static void write_data(FILE *out, struct image *image) {
    uint32_t padding_size = calculate_padding(image_interface.get_width(image));

    for (size_t i = 0; i < image_interface.get_height(image); i++) {
        size_t count = fwrite(
                image_interface.row_ptr(image, i),
                image_interface.row_size(image),
                1, out);
        if (count != 1) {
            write_data_error();
        }

        fwrite(image_interface.row_ptr(image, 0), padding_size, 1, out);
    }

}


void to_bmp(FILE *out, struct image *image) {
    write_header(out,
                 image_interface.get_width(image),
                 image_interface.get_height(image));
    write_data(out, image);
}
