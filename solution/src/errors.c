#include <stdio.h>
#include <stdlib.h>

#include "../includes/errors.h"

#define WRONG_ARGUMENTS 1
#define FILE_DOESNT_EXISTS 2
#define READ_ERROR 3
#define WRITE_ERROR 4
#define OUT_OF_MEMORY 5

void wrong_arguments_error(void) {
    printf("Arguments format:\n");
    printf("./image-transformer <source-image> <transformed-image> <angle>");
    exit(WRONG_ARGUMENTS);
}


void file_doesnt_exists_error(const char *filename) {
    printf("File %s doesn't exists", filename);
    exit(FILE_DOESNT_EXISTS);
}

void read_header_error(void) {
    printf("An error occurred while reading the header");
    exit(READ_ERROR);
}

void read_data_error(void) {
    printf("An error occurred while reading the data");
    exit(READ_ERROR);
}

void write_header_error(void) {
    printf("An error occurred while writing the header");
    exit(WRITE_ERROR);
}

void write_data_error(void) {
    printf("An error occurred while writing the data");
    exit(WRITE_ERROR);
}

void out_of_memory_error(void) {
    printf("Out of memory");
    exit(OUT_OF_MEMORY);
}



