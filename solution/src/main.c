#include "../includes/util.h"
#include "../includes/errors.h"
#include "../includes/formats/bmp/bmp.h"
#include "../includes/image.h"
#include "../includes/transformations/rotation_90_deg.h"
#include <stdlib.h>

#define ARGUMENTS_NUMBER 4
#define SOURCE_IMAGE 1
#define TRANSFORMED_IMAGE 2
#define ANGLE 3


int main(int argc, char **argv) {
    if (argc != ARGUMENTS_NUMBER) {
        wrong_arguments_error();
    }
    FILE *in = open_file(argv[SOURCE_IMAGE], READ);
    FILE *out = open_file(argv[TRANSFORMED_IMAGE], WRITE);
    struct image *source = from_bmp(in);
    struct image *transformed = rotate(source, (enum degree) atoi(argv[ANGLE]));
    to_bmp(out, transformed);
    close_file(in);
    close_file(out);
    image_interface.free(source);
    image_interface.free(transformed);

    return 0;
}
