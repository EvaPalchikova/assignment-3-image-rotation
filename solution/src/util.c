#define READ_BIN_FILE_MODE "rb"
#define WRITE_BIN_FILE_MODE "wb"

#include "../includes/util.h"
#include "../includes/errors.h"
#include <stdio.h>

FILE *open_file(const char *filename, const enum open_mode mode) {
    if (!filename) {
        wrong_arguments_error();
    }

    FILE *f = NULL;
    if (mode == READ)
        f = fopen(filename, READ_BIN_FILE_MODE);
    else if (mode == WRITE)
        f = fopen(filename, WRITE_BIN_FILE_MODE);

    if (!f) {
        file_doesnt_exists_error(filename);
    }
    return f;
}

void close_file(FILE *f) {
    fclose(f);
}
