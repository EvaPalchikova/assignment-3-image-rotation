#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ERRORS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ERRORS_H

void wrong_arguments_error(void);
void out_of_memory_error(void);
void file_doesnt_exists_error(const char *filename);
void read_header_error(void);
void read_data_error(void);
void write_header_error(void);
void write_data_error(void);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_ERRORS_H
