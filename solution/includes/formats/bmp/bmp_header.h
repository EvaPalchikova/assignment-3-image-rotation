#ifndef IMAGE_TRANSFORMER_BMP_HEADER_H
#define IMAGE_TRANSFORMER_BMP_HEADER_H

#include "../../util.h"
#include "bmp_util.h"
#include <inttypes.h>
#include <malloc.h>


#define BF_TYPE 19778
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_X_PIXELS_PER_METER 0
#define BI_Y_PIXELS_PER_METER 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0


struct bmp_header;

struct bmp_header *new_empty_bmp_header(void);

struct bmp_header *new_bmp_header(uint32_t width, uint32_t height);

uint32_t bmp_header_get_width(struct bmp_header *header);

uint32_t bmp_header_get_height(struct bmp_header *header);

size_t get_header_size(void);

void free_header(struct bmp_header *header);

static struct bmp_header_interface {

    struct bmp_header *(*create_empty)(void);

    struct bmp_header *(*create)(const uint32_t, const uint32_t);

    uint32_t (*get_width)(struct bmp_header *);

    uint32_t (*get_height)(struct bmp_header *);

    size_t (*get_size)(void);

    void (*free)(struct bmp_header *);

} const bmp_header_interface = {
        new_empty_bmp_header,
        new_bmp_header,
        bmp_header_get_width,
        bmp_header_get_height,
        get_header_size,
        free_header,
};

#endif //IMAGE_TRANSFORMER_BMP_HEADER_H
